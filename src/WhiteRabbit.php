<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //TODO implement this!
		$file = file_get_contents($filePath);
		$trimmed = preg_replace('/[^a-zA-Z0-9]/', '',$file);
		$filesend = count_chars(strtolower($trimmed),1);
		return $filesend;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //TODO implement this!
		$occurrences = max($parsedFile);
		$keyPts = array_keys($parsedFile, max($parsedFile));
		return chr($keyPts[0]);
    }
}